﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static prueba1.Juegos;

namespace prueba1
{
    public class Rpg : Juegos
    {
        public String plataforma;

        // Constructores de la clase
        public Rpg() { }

        public Rpg(String _plataforma) 
        {
            this.plataforma = _plataforma;
        }

        // setters
        public void setPlataforma(String _plataforma)
        {
            this.plataforma = _plataforma;
        }

        // getters
        public String getPlataforma()
        {
            return this.plataforma;
        }

        /// <summary>
        /// Despliega las característica del juego
        /// </summary>
        public override void caracteristica()
        {
            Console.WriteLine("Caracteristicas del Juego");
            Console.WriteLine("Titulo: " + getTitulo());
            Console.WriteLine("Genero: "+ getGenero());
        }
    }
}
