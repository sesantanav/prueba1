﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static prueba1.Juegos;

namespace prueba1
{
    public class Estrategia : Juegos
    {
        public String plataforma;

        // Constructores de la clase
        public Estrategia() { }

        public Estrategia(String _plataforma)
        {
            this.plataforma = _plataforma;
        }

        // setters
        public void setPlataforma(String _plataforma)
        {
            this.plataforma = _plataforma ;
        }

        // getters
        public String getPlataforma()
        {
            return this.plataforma;
        }

        /// <summary>
        /// Sobrescritura del método abstracto característica
        /// </summary>
        public override void caracteristica()
        {
            Console.WriteLine("Caracteristicas del Juego");
            Console.WriteLine("Titulo: " + getTitulo());
            Console.WriteLine("Genero: " + getGenero());
            Console.WriteLine("Plataforma: " + getPlataforma());
        }

    }
}
