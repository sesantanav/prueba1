﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba1
{
    /// <summary>
    /// Clase Abstracta Juegos 
    /// Definición de los atributos y características generales de los diferentes tipos
    /// de juegos.
    /// </summary>
    public abstract class Juegos
    {
        public String genero;
        public String formato;
        public String titulo;
        public String descripcion;

        // Setters genéricos
        public void setGenero(String _genero)
        {
            this.genero = _genero;
        }
        public void setFormato(String _formato)
        {
            this.formato = _formato;
        }
        public void setTiulo(String _titulo)
        {
            this.titulo = _titulo;
        }
        public void setDescripcion(String _descripcion)
        {
            this.descripcion = _descripcion;
        }
        public void setJuegos(String _genero, String _formato, String _titulo)
        {
            this.genero = _genero;
            this.formato = _formato;
            this.titulo = _titulo;
        }

        // definicion de getters
        public String getGenero()
        {
            return this.genero;
        }
        public String getFormato()
        {
            return this.formato;
        }
        public String getTitulo()
        {
            return this.titulo;
        }
        public String getDescripcion()
        {
            return this.descripcion;
        }
        public abstract void caracteristica();
        
    }
}
